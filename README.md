# Lemmy kustomize deploy

Deployment of [Lemmy](https://github.com/LemmyNet/lemmy) in Kubernetes using Kustomize.

This is very much a work in progress.

## Kustomize overlay

I've provided the ``kustomize/overlays/example`` overlay as an example, copy it and configure it for your own requirements.

## Deployment

    kustomize build kustomize/overlays/example | kubectl -n lemmy apply -f -

## Note about Persistent Volume

``kustomize/base/pvc.yaml`` is hard coded to use a Linode class. You can override this in your own overlay if you don't use Linode.

### Step 1

Create your own file called pvc.yaml and add it in your ``kustomize/overlays/example/kustomization.yaml`` under ``patchesStrategicMerge``.

```yaml
patchesStrategicMerge:
  - pvc.yaml
```

### Step 2

Then override the ``storageClassName`` in pvc.yaml like this.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-pictrs01

spec:
  storageClassName: default
```


